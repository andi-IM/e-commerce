<?php

namespace App\Http\Controllers;

use App\Jobs\MarketplaceJob;
use App\Jobs\ProductJob;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    public function index()
    {
        $product = Product::with(['category'])->orderBy('created_at', 'DESC');
        if (request()->q != '') {
            $product = $product->where('name', 'LIKE', '%' . request()->q . '%');
        }
        $product = $product->paginate(10);
        return view('products.index', compact('product'));
    }

    public function create()
    {
        $category = Category::orderBy('name', 'DESC')->get();
        return view('products.create', compact('category'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|integer',
            'weight' => 'required|integer',
            'image' => 'required|image|mimes:png,jpeg,jpg'
        ]);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . Str::slug($request->name) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/products', $filename);

            $product = Product::create([
                'name' => $request->name,
                'slug' => $request->name,
                'category_id' => $request->category_id,
                'description' => $request->description,
                'image' => $filename,
                'price' => $request->price,
                'weight' => $request->weight,
                'status' => $request->status
            ]);
            return redirect(route('product.index'))->with(['success' => 'Produk Baru Ditambahkan']);
        }
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $category = Category::orderBy('name', 'DESC')->get();
        return view('products.edit', compact('product', 'category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'description' => 'required',
            'category_id' => 'required|exist:categories,id',
            'price' => 'required|integer',
            'weight' => 'required|integer',
            'image' => 'nullable|image|mimes:png,jpeg,jpg'
        ]);

        $product = Product::find($id);
        $fileName = $product->image;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = time() . Str::slug($request->name) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/products', $fileName);
            File::delete(storage_path('app/public/products/' . $product->image));
        }

        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'price' => $request->price,
            'weight' => $request->weight,
            'image' => $fileName
        ]);
        return redirect(route('product.index'))->with(['success' => 'Data Produk Diperbarui!']);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        File::delete(storage_path('app/public/products/' . $product->image));
        $product->delete();
        return redirect(route('product.index'))->with(['success' => 'Produk telah dihapus!']);
    }

    public function massUploadForm()
    {
        $category = Category::orderBy('name','DESC')->get();
        return view('products.bulk', compact('category'));
    }



    public function massUpload(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|exist:categories,id',
            'file' => 'required|mimes:xlsx'
        ]);

        if ($request->hasFile('file')){
            $file = $request->file('file');
            $fileName = time() . '-product.' . $file->getClientOriginalExtension();
            $file->storeAs('public/uploads', $fileName);

            ProductJob::dispatch($request->category_id, $fileName);
            return redirect()->back()->with(['success' => 'Upload Produk dijadwalkan']);
        }
    }

    public function uploadViaMarketplace(Request $request)
    {
        $this->validate($request, [
            'marketplace' => 'required|string',
            'username' => 'required|string'
        ]);

        MarketplaceJob::dispatch($request->username, 10);
        return redirect()->back()->with(['success' => 'Produk Dalam Antrian']);
    }

}
